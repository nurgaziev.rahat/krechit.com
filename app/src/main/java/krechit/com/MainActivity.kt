package krechit.com

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView
import krechit.com.databinding.ActivityMainBinding
import krechit.com.databinding.AppBarMainBinding

class MainActivity : AppCompatActivity() {
    //region values
    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding
    private lateinit var includeBinding: AppBarMainBinding
    private lateinit var toolbar: Toolbar
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var navView : NavigationView
    private lateinit var navController : NavController
    private lateinit var bottomNav : BottomNavigationView
    //endregion

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        includeBinding = AppBarMainBinding.bind(binding.appBar.root)
        setContentView(binding.root)
        intiViews()
        setupNavigation()
    }

    private fun intiViews(){
        navController = findNavController(R.id.nav_host_fragment)

        includeBinding.also {
            toolbar = it.toolbar
            bottomNav = it.bottomNav
        }
        binding.also {
            drawerLayout = it.drawerLayout
            navView = it.navView
        }
    }

    private fun setupNavigation(){
        setSupportActionBar(toolbar)
        appBarConfiguration = AppBarConfiguration(navController.graph,drawerLayout)
        bottomNav.setupWithNavController(navController)
        setupActionBarWithNavController(navController,appBarConfiguration)
        navView.setupWithNavController(navController)
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

}