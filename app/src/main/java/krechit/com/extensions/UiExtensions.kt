package krechit.com.extensions

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide
import krechit.com.R

fun ImageView.load(context: Context, url: String?){
    Glide.with(context)
        .asBitmap()
        .placeholder(R.drawable.placeholder)
        .circleCrop()
        .load(url)
        .into(this)
}